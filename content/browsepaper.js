// This file is part of Browsepaper.

// Browsepaper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Browsepaper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Browsepaper.  If not, see <http://www.gnu.org/licenses/>.

var Browsepaper = (function () {
    "use strict";

    let CC = Components.classes;
    let CI = Components.interfaces;

    Components.utils.import("resource://gre/modules/FileUtils.jsm");

    let Strings = null;
    let Prompt = CC["@mozilla.org/embedcomp/prompt-service;1"]
	.getService(CI.nsIPromptService);

    let backgroundPage = "chrome://browsepaper/content/background.html";

    let o = {};

    o.saveToName = "browsepaper-wallpaper"

    o.BrowserOpenTab = function () {
	if (o.getExistingWallpaper() === "") {
	    o.oldBrowserOpenTab();
	    return;
	}
	
	let newtab = gBrowser.loadOneTab(backgroundPage,
					 null, null, null,
					 false, false);
	newtab.linkedBrowser.userTypedValue = undefined;

	if (gURLBar) {
	    setTimeout(function () {gURLBar.value = ""; gURLBar.focus();}, 1);
	}
    };

    o.getTabTitle = function () {
	let o;

	try {
	    o = document.getAnonymousElementByAttribute
	    (document.getElementById("content"),
	     "anonid",
	     "tbstringbundle").getString("tabs.untitled");
	} catch (e) {
	    o = document.getAnonymousElementByAttribute
	    (document.getElementById("content"),
	     "anonid",
	     "tbstringbundle").getString("tabs.emptyTabTitle");
	}

	return o;
    };

    o.getExistingWallpaper = function () {
	let dir = FileUtils.getDir("ProfD", []);
	let entries = dir.directoryEntries;
	while (entries.hasMoreElements() == true) {
	    let e = entries.getNext();
	    e.QueryInterface(CI.nsIFile);
	    if (e.leafName.lastIndexOf(o.saveToName) == 0) {
		return e.leafName;
	    }
	}

	return "";
    };

    o.onContentLoad = function (event) {
	if (event.originalTarget instanceof HTMLDocument == false) {
	    return;
	}

	let IOS = CC["@mozilla.org/network/io-service;1"]
	    .getService(CI.nsIIOService);

	let doc = event.originalTarget;

	if (doc.defaultView.location.toString().search(backgroundPage) === -1) {
	    return;
	}

	doc.title = o.getTabTitle();

	let file = FileUtils.getFile("ProfD", [o.getExistingWallpaper()]);
	let url = (IOS.newFileURI(file)).spec;

	doc.styleSheets[0].insertRule("body { "
				      +"background-image: url("+url+");"
				      +"background-size: 100% 100%;"
				      +"background-position: center;"
				      +"background-repeat: no-repeat;"
				      +"overflow: hidden;"
				      +"}",
				      doc.styleSheets[0].cssRules.length);
    };

    o.onURLBarFocus = function () {
	if (gURLBar.value === backgroundPage) {
	    gURLBar.value = "";
	}
    };

    o.onTabEvent = function (e) {
	if (gURLBar.value === backgroundPage) {
	    gURLBar.value = "";
	}
    }

    o.init = function () {
	gBrowser.removeEventListener("NewTab", window.BrowserOpenTab, false);
	o.oldBrowserOpenTab = window.BrowserOpenTab;
	window.BrowserOpenTab = o.BrowserOpenTab;
	gBrowser.addEventListener("NewTab", window.BrowserOpenTab, false);
	if (gURLBar) {
	    gURLBar.addEventListener("focus", o.onURLBarFocus, false);

	    gBrowser.tabContainer.addEventListener("TabSelect",
						   o.onTabEvent, false);
	}
    };

    o.onLoad = function (event) {
	o.init();
	gBrowser.addEventListener("DOMContentLoaded", o.onContentLoad, true);
	Strings = document.getElementById("browsepaper-strings");
    };

    o.openFileChooser = function () {
	let FilePicker = CC["@mozilla.org/filepicker;1"]
	    .createInstance(CI.nsIFilePicker);

	FilePicker.init(window, "Pick a Background", CI.nsIFilePicker.modeOpen);
	FilePicker.appendFilters(CI.nsIFilePicker.filterImages);
	FilePicker.open(function (code) {
	    if (code == CI.nsIFilePicker.returnCancel) {
		return;
	    }

	    let picked = FilePicker.file;
	    let copyext = picked.leafName.slice(
		(picked.leafName.lastIndexOf(".")-1 >>> 0) + 2
	    );
	    if (copyext === "") {
		Prompt.alert(window,
			     Strings.getString("error"),
			     Strings.getString("invalidExtension"));
	    }
	    let fileName = o.saveToName+"."+copyext;
	    
	    let dir = FileUtils.getDir("ProfD", []);

	    picked.copyTo(dir, fileName);

	    Prompt.alert(window,
			 Strings.getString("success"),
			 Strings.getString("success"));
	});
    };

    o.deleteBackground = function () {
	if (o.getExistingWallpaper() != "") {
	    FileUtils.getFile("ProfD",
			      [o.getExistingWallpaper()]).remove(false);
	    Prompt.alert(window,
			 Strings.getString("success"),
			 Strings.getString("success"));
	} else {
	    Prompt.alert(window,
			 Strings.getString("error"),
			 Strings.getString("nowall"));
	}
    };

    return o;
})();

window.addEventListener("load", Browsepaper.onLoad, false);
